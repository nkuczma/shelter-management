import { ShelterMaintenancePage } from './app.po';

describe('shelter-maintenance App', () => {
  let page: ShelterMaintenancePage;

  beforeEach(() => {
    page = new ShelterMaintenancePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
