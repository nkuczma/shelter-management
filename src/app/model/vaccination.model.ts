export class Vaccination {
    
        constructor(
            public animal_id: number,
            public vacc_id: number,
            public vacc_name: string,
            public vacc_date: Date,
            public next_vacc: Date,
        ) { }
    }