export class Animal {
    
        constructor(
            public animal_id: number,
            public registry_number: number, 
            public tatoo_id: string,
            public microchip: string, 
            public species: string,
            public state: string,
            public for_adoption: boolean,
            public felv_test: boolean,
            public age: number,
            public gender: string,
            public size: string,
            public enter_date: Date,
            public leave_date: Date,
            public quarantine: boolean,
            public name: string,
            public description: string
        ) { }
    }