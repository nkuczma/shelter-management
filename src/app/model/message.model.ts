export class Message {
    
    constructor(
        public message_id: number,
        public shelter_id: number,
        public animal_id: number,
        public user_name: string,
        public user_mail: string,
        public user_phone: number,
        public message_content: string,
        public date_send: Date,
        public date_end: Date,
        public user_id: number,
        public comment: string,
    ) { }
}