export class Treatment {
    
        constructor(
            public treatment_id: number,
            public animal_id: number,
            public treatment_date: Date,
            public medicine: string,
            public is_treated: boolean,
            public next_med: Date,
            public treat_name: string
        ) { }
    }