export class Shelter {
    
    constructor(
        shelter_id: number,
        name_full: string,
        name_short: string,
        city: string,
        province: number,
        address: string,
        phone: number,
        zipcode: string
    ) { }
}