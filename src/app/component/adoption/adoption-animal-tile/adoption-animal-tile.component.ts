import { Component, OnInit, Input } from '@angular/core';
import { Animal } from "app/model/animal.model";
import { PhotoService } from 'app/service/photo.service';

@Component({
  selector: 'app-adoption-animal-tile',
  templateUrl: './adoption-animal-tile.component.html',
  styleUrls: ['./adoption-animal-tile.component.scss']
})
export class AdoptionAnimalTileComponent implements OnInit {

  constructor( private photoService: PhotoService ) { }

  @Input() animal: Animal;
  imgUrl: string;
  dict : object = {
    "dog": "pies",
    "cat": "kot",
    "m": "męska",
    "f": "żeńska",
    "small": "mały",
    "medium": "średni",
    "big": "duży",
  }

  ngOnInit() {
    this.getPhoto(this.animal.animal_id);
  }

  getPhoto(id){
    this.photoService.getAnimalPhoto(id)
    .then((response) => { this.imgUrl = response.json();  return response;})
    .catch((response) => { this.imgUrl = "./src/assets/img/no-photo.svg"; console.log('no photo'); });
  }
}
