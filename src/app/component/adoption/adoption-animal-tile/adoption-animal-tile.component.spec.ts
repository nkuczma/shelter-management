import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionAnimalTileComponent } from './adoption-animal-tile.component';

describe('AdoptionAnimalTileComponent', () => {
  let component: AdoptionAnimalTileComponent;
  let fixture: ComponentFixture<AdoptionAnimalTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionAnimalTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionAnimalTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
