import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AnimalService } from 'app/service/animal.service';
import { PhotoService } from 'app/service/photo.service';

@Component({
  selector: 'app-adoption-animal-details',
  templateUrl: './adoption-animal-details.component.html',
  styleUrls: ['./adoption-animal-details.component.scss']
})
export class AdoptionAnimalDetailsComponent implements OnInit {

  constructor( 
    private route: ActivatedRoute,
    private animalService: AnimalService,
    private photoService: PhotoService
  ) { }

  animalId: number;
  imgUrl: string;
  animal: object;
  dict : object = {
    "dog": "pies",
    "cat": "kot",
    "m": "męska",
    "f": "żeńska",
    "small": "mały",
    "medium": "średni",
    "big": "duży",
    "true": "pozytywny",
    "false": "negatywny"
  }

  ngOnInit() {
    this.route.params.
    subscribe( params => {
      this.animalId = params['animalId'];
    });
    this.getAnimalInfo(this.animalId);
    this.getPhoto(this.animalId)
  }
  getAnimalInfo(id) {
    this.animalService.getAnimalAdoption(id)
      .then((response) => { this.animal = response[0]; console.log(this.animal); return response;})
      .catch((response) => { console.log('error'); });
  }
  getPhoto(id){
    this.photoService.getAnimalPhoto(id)
      .then((response) => { this.imgUrl = response.json(); return response;})
      .catch((response) => { this.imgUrl = "./src/assets/img/no-photo.svg"; console.log('no photo'); });
  }
}
