import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionAnimalDetailsComponent } from './adoption-animal-details.component';

describe('AdoptionAnimalDetailsComponent', () => {
  let component: AdoptionAnimalDetailsComponent;
  let fixture: ComponentFixture<AdoptionAnimalDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionAnimalDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionAnimalDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
