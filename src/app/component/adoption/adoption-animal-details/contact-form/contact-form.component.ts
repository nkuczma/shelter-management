import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from 'app/service/message.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  constructor( private messageService: MessageService ) { }

  @Input() animalId: number;
  @Input() shelterId: number;
  wasSend: boolean = false;
  isSuccess: boolean = false;
  disableSubmit: boolean = false;

  ngOnInit() {
  }

  onSubmit(messageForm) {
    this.disableSubmit = true;
    let data = messageForm.value;
    data.shelter_id = this.shelterId;
    data.animal_id = this.animalId;
    data.date_send = new Date();
    this.messageService.postMessage(data)
      .then((response) => { this.wasSend = true; this.isSuccess = true; this.disableSubmit = false; return response; })
      .catch((response) => { this.wasSend = true; this.isSuccess = false; this.disableSubmit = false; return response; })
  }

}
