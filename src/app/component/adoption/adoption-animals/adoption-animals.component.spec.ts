import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptionAnimalsComponent } from './adoption-animals.component';

describe('AdoptionAnimalsComponent', () => {
  let component: AdoptionAnimalsComponent;
  let fixture: ComponentFixture<AdoptionAnimalsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptionAnimalsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptionAnimalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
