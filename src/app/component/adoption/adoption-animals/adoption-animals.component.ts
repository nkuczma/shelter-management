import { Component, OnInit } from '@angular/core';
import { ShelterService } from 'app/service/shelter.service';
import { AnimalService } from 'app/service/animal.service';
import { Animal } from "app/model/animal.model";

@Component({
  selector: 'app-adoption-animals',
  templateUrl: './adoption-animals.component.html',
  styleUrls: ['./adoption-animals.component.scss']
})
export class AdoptionAnimalsComponent implements OnInit {

  constructor( private shelterService: ShelterService, private animalService: AnimalService ) { }

  provinces: object[];
  animals : Animal[];
  animalList : Animal[];
  provinceFilter: string = "all";
  speciesFilter: string = "all";
  sizeFilter: string = "all";
  genderFilter: string = "all";
  
  noData: boolean = false;

  ngOnInit() {
    this.getProvinces();
  }

  getProvinces() {
    this.shelterService.getProvinceList()
    .then((response) => { this.provinces = response.json(); return response;})
    .catch((response) => { console.log('error'); });
  }

  setSpeciesFilter(val) {
    this.speciesFilter = val;
  }
  setProvinceFilter(val) {
    this.provinceFilter = val;
  }
  setSize(val) {
    this.sizeFilter = val;
    this.filter();
  }
  setGender(val) {
    this.genderFilter = val;
    this.filter();
  }
  getAnimals() {
    this.animalService.getAnimalsBySpeciesProvince(this.provinceFilter, this.speciesFilter)
      .then((response) => { this.animalList = response; this.animals = this.animalList; this.filter(); this.noData = false; return response;})
      .catch((response) => { this.noData = true; this.animals=[]; console.log(response); return response;});
  }
  filter() {
    let animalsFiltered;

    animalsFiltered = this.animalList.filter((val)=> {
      if (this.sizeFilter==="all") return val;
      else if (val.size && val.size === this.sizeFilter) return val;
    });

    animalsFiltered = animalsFiltered.filter((val)=> {
      if (this.genderFilter==="all") return val;
      else if (val.gender && val.gender === this.genderFilter) return val;
    });

    this.animals = animalsFiltered;
  }
}
