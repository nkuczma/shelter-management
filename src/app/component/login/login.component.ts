import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from "../../model/user.model";
import { UserData } from "../../model/user-data.model";
import { AuthService } from "../../service/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  wasError: boolean = false;

  ngOnInit() {
  }

  onSubmit(loginForm: NgForm) {
    if(loginForm.valid) { 
      let toSend = loginForm.value; 
      if (toSend !== undefined) {
      this.authService.login(toSend)
        .then((response) => { this.handleLogin(response.json()); return response._body;})
        .catch((response) => { this.handleError(response._body); this.wasError = true; return response._body; });
      }
    }
    else { console.log('blad');}
  }

  handleLogin(res) {
    console.log(res);
    console.log(res.id_token);
    localStorage.setItem('id_token',res.id_token);
    this.authService.setUserData({shelter_id: res.shelter_id, user_name: res.user_name, user_role: res.role});
    this.router.navigate(['/manage/home']);
    this.authService.isLogged = true;
  }

  handleError(res) {
    console.log('blad');
  }

}
