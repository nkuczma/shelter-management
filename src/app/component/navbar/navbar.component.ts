import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../service/auth.service";

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthService) { }
  isAdmin: boolean;

  ngOnInit() {
    console.log(this.authService.getUserData());
    let user = this.authService.getUserData();
    this.isAdmin = user.user_role === "admin"? true : false;
  }
  logout(){
    this.authService.logout();
  }
}
