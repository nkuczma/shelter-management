import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../service/auth.service";
import { UserData } from "app/model/user-data.model";
import { NotificationService } from 'app/service/notification.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor( private authService: AuthService, private notificationService: NotificationService ) { }

  user: UserData;
  missingInfo: object[];
  nextVacc: object[];
  nextTreat: object[];
  missingInfoVals : object = {
    "tatoo_id": "numer tatuażu",
    "microchip": "numer mikroczipu",
    "species": "gatunek",
    "state": "stan",
    "for_adoption": "możliwość adopcji",
    "felv_test": "test felv",
    "age": "wiek",
    "gender": "płeć",
    "size": "rozmiar",
    "description": "opis",
    "image_url": "zdjęcie"
  }


  ngOnInit() {
    this.user = this.authService.getUserData();
    this.getMissingInfo(this.user.shelter_id);
    this.getNextVacc(this.user.shelter_id);
    this.getNextTreat(this.user.shelter_id);
  }

  getMissingInfo(id: number) {
    this.notificationService.getMissingInfo(id)
      .then((response)=> { this.missingInfo = response.json(); return response; })
      .catch((response)=> { console.log('error missing'); });
  }

  getNextVacc(id: number) {
    this.notificationService.getNextVaccinations(id)
      .then((response)=> { this.nextVacc = response.json(); return response; })
      .catch((response)=> { console.log('error vacc'); });
  }

  getNextTreat(id: number) {
    this.notificationService.getNextTreatments(id)
      .then((response)=> { this.nextTreat = response.json(); return response; })
      .catch((response)=> { console.log('error treat'); });
  }

}
