import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../service/auth.service";
import { UserData } from "../../model/user-data.model";
import { Router } from "@angular/router";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }
  userData: UserData;

  ngOnInit() {
    this.userData = this.authService.getUserData();
    console.log(this.userData);
    if(this.userData.user_role==="admin") {
      // this.router.navigate(['/manage/user-add']);
    }
  }

}
