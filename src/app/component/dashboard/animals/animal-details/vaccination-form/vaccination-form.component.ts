import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AnimalService } from 'app/service/animal.service';

@Component({
  selector: 'vaccination-form',
  templateUrl: './vaccination-form.component.html',
  styleUrls: ['./vaccination-form.component.scss']
})
export class VaccinationFormComponent implements OnInit {

  constructor( private animalService: AnimalService ) { }

  @Input() animalId: number;
  @Output() onFormSubmit = new EventEmitter<boolean>();
  disableSubmit: boolean = false;

  ngOnInit() {
  }

  onSubmit(vaccinationForm: NgForm) {
    this.disableSubmit = true;
    this.animalService.addVaccination(vaccinationForm.value, this.animalId)
    .then((response) => { this.onFormSubmit.emit(true); this.disableSubmit = false; return response; })
    .catch((response) => { this.onFormSubmit.emit(false); this.disableSubmit = false; return response; });
  }

}
