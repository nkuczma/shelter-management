import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AnimalService } from 'app/service/animal.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'treatment-form',
  templateUrl: './treatment-form.component.html',
  styleUrls: ['./treatment-form.component.scss']
})
export class TreatmentFormComponent implements OnInit {

  constructor( private animalService: AnimalService ) { }

  @Input() animalId: number;
  @Output() onFormSubmit = new EventEmitter<boolean>();
  disableSubmit: boolean = false;

  ngOnInit() {
  }

  onSubmit(treatmentForm: NgForm) {
    this.disableSubmit = true;
    this.animalService.addTreatment(treatmentForm.value, this.animalId)
    .then((response) => { this.onFormSubmit.emit(true); this.disableSubmit = false; return response; })
    .catch((response) => { this.onFormSubmit.emit(false); this.disableSubmit = false; return response; });
  }

}
