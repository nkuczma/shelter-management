import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { AnimalService } from 'app/service/animal.service';
import { PhotoService } from 'app/service/photo.service';
import { ShelterService } from 'app/service/shelter.service';
import { DocumentsService } from 'app/service/documents.service';
import { Animal } from "app/model/animal.model";
import { NgForm } from '@angular/forms';
import { Vaccination } from 'app/model/vaccination.model'
import { Treatment } from 'app/model/treatment.model';
import { Shelter } from 'app/model/shelter.model';
import { saveAs } from 'file-saver/FileSaver';

@Component({
  selector: 'app-animal-details',
  templateUrl: './animal-details.component.html',
  styleUrls: ['./animal-details.component.scss']
})
export class AnimalDetailsComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private animalService: AnimalService,
    private photoService: PhotoService,
    private shelterService: ShelterService,
    private documentsService: DocumentsService
  ) { }

  shelterId: number;
  animalId: number;
  animal: Animal;
  shelter: Shelter;
  vaccinations: Vaccination [];
  treatments: Treatment [];
  showVaccForm: boolean = false;
  showTreatForm: boolean = false;
  showInfoEditForm: boolean = false;
  showFullForm: boolean = false;
  showVaccEdit: boolean = false;
  showTreatEdit: boolean = false;
  showNameEdit: boolean = false;
  showDescForm: boolean = false;
  showImgForm: boolean = false;
  wasError: boolean;
  wasSend: boolean = false;
  wasTreatError: boolean;
  wasTreatSend: boolean = false;
  disableSubmit: boolean = false;
  editTreatmentId: number;
  editVaccinationId: number;
  currentVacc: Vaccination;
  currentTreat: Treatment;
  imgUrl: string;
  file: File;
  dict : object = {
    "dog": "pies",
    "cat": "kot",
    "m": "męska",
    "f": "żeńska",
    "small": "mały",
    "medium": "średni",
    "big": "duży"
  }

  ngOnInit() {
    this.route.params.
      subscribe( params => {
        this.shelterId = params['shelterId'];
        this.animalId = params['animalId'];
        
      });
    this.getAnimal(this.animalId);
    this.getShelter(this.shelterId);
    this.getVaccinations(this.animalId);
    this.getTreatments(this.animalId);
    this.getPhoto(this.animalId);

  }
  getAnimal(id){
    this.animalService.getAnimalByAnimalId(id)
    .then((response) => { this.animal = response[0]; return response;})
    .catch((response) => { console.log('error'); });
  }
  getShelter(id) {
    this.shelterService.getShelter(id)
    .then((response) => { this.shelter = response.json(); console.log(this.shelter); return response;})
    .catch((response) => { console.log('error'); });
  }
  getVaccinations(id){
    this.animalService.getVaccinationByAnimalId(id)
    .then((response) => { this.vaccinations = response; return response;})
    .catch((response) => { console.log('error'); });
  }
  getTreatments(id){
    this.animalService.getTreatmentByAnimalId(id)
    .then((response) => { this.treatments = response; return response;})
    .catch((response) => { console.log('error'); });
  }
  getPhoto(id){
    this.photoService.getAnimalPhoto(id)
    .then((response) => { this.imgUrl = response.json(); this.showImgForm = false; return response;})
    .catch((response) => { this.imgUrl = "./src/assets/img/no-photo.svg"; console.log('no photo'); });
  }

  onVaccFormSubmit(success: boolean) {
    this.wasSend = true;
    if(success) { this.showVaccForm = false; this.getVaccinations(this.animalId); this.wasError = false; }
    else { this.wasError = true; }
  }
  onTreatFormSubmit(success: boolean) {
    this.wasTreatSend = true;
    if(success) { this.showTreatForm = false; this.getTreatments(this.animalId); this.wasTreatError = false; }
    else { this.wasTreatError = true; }
  }

  onSubmit(form: NgForm) {
    this.disableSubmit = true;
    this.animalService.editAnimal(form.value, this.animalId)
      .then((response) => { this.disableSubmit = false; this.getAnimal(this.animalId); this.showInfoEditForm = false; this.showFullForm = false; this.showNameEdit = false; this.showDescForm = false; return response; })
      .catch((response) => { console.log('blad'); this.disableSubmit = false; return response; });
  }

  onSubmitVacc(vaccinationForm: NgForm) {
    this.disableSubmit = true;
    this.animalService.editVaccination(vaccinationForm.value, this.editVaccinationId)
    .then((response) => { this.getVaccinations(this.animalId); this.showVaccEdit=false; return response; })
    .catch((response) => { return response; });
  }

  editVaccination(id) {
    this.showVaccEdit = true;
    this.editVaccinationId = id;
    this.currentVacc = this.vaccinations.filter((val, key) => { return val.vacc_id === this.editVaccinationId; })[0];
  }

  editTreatment(id) {
    this.showTreatEdit = true;
    this.editTreatmentId = id;
    this.currentTreat = this.treatments.filter((val, key) => { return val.treatment_id === this.editTreatmentId; })[0];
    console.log(this.currentTreat);
  }

  onSubmitTreat(treatmentForm: NgForm) {
    this.animalService.editTreatment(treatmentForm.value, this.editTreatmentId)
    .then((response) => { this.getTreatments(this.animalId); this.showTreatEdit=false; return response; })
    .catch((response) => { return response; });
  }
  onChange(event) {
    var files = event.target.files;
    console.log(files[0].size);
    if(files[0].size > 4002776) {
      alert('Za duży rozmiar pliku');
    }
    else {
      this.file = files[0];
    }
  }
  onSubmitPhoto() {
    if(this.file) {
      var formData: FormData = new FormData();
      formData.append('files',this.file);
      this.photoService.postAnimalPhoto(formData, this.animalId)
        .then((response) => { console.log(response); this.getPhoto(this.animalId); this.file=null; return response;})
        .catch((response) => { console.log(response); });
      }
  }

  onGetAnimalDocument() {
    this.documentsService.getAnimalDocument(this.animal, this.shelter)
      .subscribe(res => {
        saveAs(res,"umowa.docx");
        let fileURL = URL.createObjectURL(res);
        window.open(fileURL);
      });
  }

}
