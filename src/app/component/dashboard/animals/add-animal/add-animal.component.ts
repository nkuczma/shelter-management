import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AnimalService } from 'app/service/animal.service';
import { PhotoService } from 'app/service/photo.service';

@Component({
  selector: 'app-add-animal',
  templateUrl: './add-animal.component.html',
  styleUrls: ['./add-animal.component.scss']
})
export class AddAnimalComponent implements OnInit {

  constructor( private animalService: AnimalService, private photoService: PhotoService ) { }

  isSuccess: boolean = false;
  wasSend: boolean = false;
  disableSubmit: boolean = false;
  file: File;

  ngOnInit() {
  }

  onChange(event) {
    var files = event.target.files;
    console.log(files[0].size);
    if(files[0].size > 4002776) {
      alert('Za duży rozmiar pliku');
    }
    else {
      this.file = files[0];
    }
  }

  onSubmit(animalForm: NgForm) {
    this.disableSubmit = true;
    console.log(animalForm);
    this.animalService.addAnimal(animalForm.value)
    .then((response) => {
      animalForm.reset();
      this.isSuccess = true;
      this.wasSend = true;
      this.disableSubmit = false;
      let animalId = response.json().animal_id;
      console.log(this.file);
      if(this.file) {
        var formData: FormData = new FormData();
        formData.append('files',this.file);
        this.photoService.postAnimalPhoto(formData, animalId)
          .then((response) => { console.log(response); this.file=null; return response;})
          .catch((response) => { console.log(response); });
        }
      return response; 
    })
    .catch((response) => { this.isSuccess = false; this.wasSend = true; this.disableSubmit = false; return response; });
  }

}
