import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from "app/service/auth.service";
import { Animal } from "app/model/animal.model";
import { PhotoService } from 'app/service/photo.service';

@Component({
  selector: 'app-animal-tile',
  templateUrl: './animal-tile.component.html',
  styleUrls: ['./animal-tile.component.scss']
})
export class AnimalTileComponent implements OnInit {

  @Input() animal: Animal;
  shelterId: number;
  imgUrl: string;
  
  constructor( private authService: AuthService, private photoService: PhotoService ) { }

  ngOnInit() {
    this.shelterId = this.authService.getUserData().shelter_id;
    this.getPhoto(this.animal.animal_id);
  }

  getPhoto(id){
    this.photoService.getAnimalPhoto(id)
    .then((response) => { this.imgUrl = response.json(); return response;})
    .catch((response) => { this.imgUrl = "./src/assets/img/no-photo.svg"; console.log('no photo'); });
  }
}
