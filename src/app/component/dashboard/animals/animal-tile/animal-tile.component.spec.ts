import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalTileComponent } from './animal-tile.component';

describe('AnimalTileComponent', () => {
  let component: AnimalTileComponent;
  let fixture: ComponentFixture<AnimalTileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimalTileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalTileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
