import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/service/auth.service';
import { AnimalService } from 'app/service/animal.service';
import { Animal } from "app/model/animal.model";
import { UserData } from "app/model/user-data.model";
import { Http } from '@angular/http';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.scss']
})
export class AnimalsComponent implements OnInit {

  userData: UserData;
  animals : Animal[];
  animalList : Animal[];
  stateFilter : string;
  searchFilter: string;
  speciesFilter: string;


  constructor( private authService: AuthService, private animalService: AnimalService, private http: Http ) { }
  
  ngOnInit() {
    this.userData = this.authService.getUserData();
    this.getAnimals(this.userData.shelter_id);
    this.stateFilter = "all";
    this.speciesFilter = "all";
    this.searchFilter = "";
  }

  getAnimals(id) {
    this.animalService.getAnimalsByShelterId(id)
    .then((response) => { this.animalList = response; this.animals = this.animalList; return response;})
    .catch((response) => { console.log('error'); });
  }

  filter() {
    let animalsFiltered;

    animalsFiltered = this.animalList.filter((val)=> {
      if (val.name.toLowerCase().includes(this.searchFilter.toLowerCase())) return val;
      else if (val.registry_number && val.registry_number.toString().includes(this.searchFilter)) return val;
      else if (val.microchip && val.microchip.toLowerCase().includes(this.searchFilter.toLowerCase())) return val;
      else if (val.tatoo_id && val.tatoo_id.toLowerCase().includes(this.searchFilter.toLowerCase())) return val;
    });

    animalsFiltered = animalsFiltered.filter((val)=> {
      if (this.stateFilter==="all") return val;
      else if (val.state && val.state === this.stateFilter) return val;
    });

    animalsFiltered = animalsFiltered.filter((val)=> {
      if (this.speciesFilter==="all") return val;
      else if (val.species && val.species === this.speciesFilter) return val;
    });

    this.animals = animalsFiltered;
  }

  setSearchFilter(value: string) {
    this.searchFilter = value;
    this.filter();
  }
  setStateFilter(value: string) {
    this.stateFilter = value;
    this.filter();
  }

  setSpeciesFilter(value: string) {
    this.speciesFilter = value;
    this.filter();
  }

}
