import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from "rxjs/Rx";
import { AuthService } from "../../../service/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }

  isSuccess: boolean = false;
  wasSend: boolean = false;
  duplicateUser: boolean = false;
  disableSubmit: boolean = false;

  ngOnInit() {
  }

  onSubmit(registerForm: NgForm) {
    console.log(registerForm.value);
    this.disableSubmit = true;
    this.wasSend = true;
    if(registerForm.valid) {
      let toSend = {
      'user_name': registerForm.value.user_name,
      'password': registerForm.value.password,
      'token': localStorage.getItem('id_token')
    }
    if (toSend !== undefined) {
      this.authService.createUser(toSend)
        .then((response) => { this.isSuccess = true; this.duplicateUser = false; registerForm.reset(); this.disableSubmit = false; return response;})
        .catch((response) => { this.handleError(response._body); this.disableSubmit = false; return response; });
    }
    }
    else { this.isSuccess = false; }
  }
  handleError(res) {
    this.isSuccess = false;
    if(res === "duplicate username" ) { this.duplicateUser = true; }
  }
}
