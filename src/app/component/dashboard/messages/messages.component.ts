import { Component, OnInit } from '@angular/core';
import { UserData } from 'app/model/user-data.model';
import { Message } from 'app/model/message.model';
import { AuthService } from 'app/service/auth.service';
import { MessageService } from 'app/service/message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor( private authService: AuthService, private messageService: MessageService) { }

  user: UserData;
  messages: Message[];
  userMessages: Message[];
  currentMessageId: number;
  currentMessage: Message;
  showEditMessage: boolean = false;
  isAdmin: boolean = false;
  allMessages: Message[];

  ngOnInit() {
    this.user = this.authService.getUserData();
    this.getMessages(this.user.shelter_id);
    this.getUserMessages();
    if(this.user.user_role==="admin") {
      this.isAdmin = true;
      this.getAllMessages(this.user.shelter_id);
    }
  }
  getMessages(id) {
    this.messageService.getUnansweredMessages(id)
      .then((response) => { this.messages = response.json(); return response;})
      .catch((response) => { console.log(response); return response;});
  }
  getUserMessages() {
    var data: any = {};
    data.token = localStorage.getItem('id_token');
    this.messageService.getUserMessages(data)
      .then((response) => {this.userMessages = response.json(); console.log(this.userMessages); return response;})
      .catch((response) => { console.log(response); return response;} );
  }
  getAllMessages(id) {
    this.messageService.getAllMessages(id)
      .then((response) => { this.allMessages = response.json(); return response;})
      .catch((response) => { console.log(response); return response;});
  }
  claimMessage(id) {
    var data: any = {};
    data.token = localStorage.getItem('id_token');
    data.date_end = null;
    data.comment = null;
    console.log(data);
    this.messageService.editMessage(data, id)
      .then((response) => { this.getMessages(this.user.shelter_id); this.getUserMessages(); return response;})
      .catch((response) => { console.log(response); return response;});
  }
  editMessageClick(id) {
    this.currentMessageId = id;
    this.showEditMessage = true;
    this.currentMessage = this.userMessages.filter((val)=> {if (val.message_id===id) return val; })[0];
  }

  submitMessageEdit(messageForm) {
    let data = messageForm.value;
    data.token = localStorage.getItem('id_token');
    console.log(data);
    this.messageService.editMessage(data, this.currentMessageId)
      .then((response) => { this.getUserMessages(); this.showEditMessage = false; return response;})
      .catch((response) => { console.log(response); return response;});
  }
}
