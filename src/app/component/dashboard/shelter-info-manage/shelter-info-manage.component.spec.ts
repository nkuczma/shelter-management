import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShelterInfoManageComponent } from './shelter-info-manage.component';

describe('ShelerInfoManageComponent', () => {
  let component: ShelterInfoManageComponent;
  let fixture: ComponentFixture<ShelterInfoManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShelterInfoManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShelterInfoManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
