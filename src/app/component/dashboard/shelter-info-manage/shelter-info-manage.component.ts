import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../service/auth.service";
import { UserData } from "app/model/user-data.model";
import { Shelter } from "app/model/shelter.model";
import { ShelterService } from 'app/service/shelter.service';

@Component({
  selector: 'app-shelter-info-manage',
  templateUrl: './shelter-info-manage.component.html',
  styleUrls: ['./shelter-info-manage.component.scss']
})
export class ShelterInfoManageComponent implements OnInit {

  constructor(private shelterService: ShelterService, private authService: AuthService) { }

  user: UserData;
  shelter: Shelter;
  isSuccess: boolean = false;
  wasSend: boolean = false;
  disableSubmit: boolean = false;
  provinces: object[];

  ngOnInit() {
    this.user = this.authService.getUserData();
    this.getShelter(this.user.shelter_id);
    this.getProvinces();
  }
  getShelter(id) {
    this.shelterService.getShelter(id)
    .then((response) => { this.shelter = response.json(); console.log(this.shelter); return response;})
    .catch((response) => { console.log('error'); });
  }
  getProvinces() {
    this.shelterService.getProvinceList()
    .then((response) => { this.provinces = response.json(); return response;})
    .catch((response) => { console.log('error'); });
  }
  onSubmit(shelterForm) {
    this.disableSubmit = true;
    console
    this.shelterService.editShelter(shelterForm.value,this.user.shelter_id)
    .then((response) => {
      this.isSuccess = true;
      this.wasSend = true;
      this.disableSubmit = false;
      this.getShelter(this.user.shelter_id);
      return response; 
    })
    .catch((response) => { console.log(response); this.isSuccess = false; this.wasSend = true; this.disableSubmit = false; return response; });

  }

}
