import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { routes } from "./app.routes";

import { AuthService } from "app/service/auth.service";
import { AnimalService } from "app/service/animal.service";
import { PhotoService } from "app/service/photo.service";
import { ShelterService } from "app/service/shelter.service";
import { NotificationService } from "app/service/notification.service";
import { MessageService } from "app/service/message.service";
import { DocumentsService } from 'app/service/documents.service';

import { AuthGuard } from "app/guard/auth.guard";
import { AdminGuard } from "app/guard/admin.guard";
import { AnimalDetailsGuard } from './guard/animal-details.guard';

import { AnimalStatePipe } from './pipes/animal-state/animal-state.pipe';
import { AnimalTypePipe } from './pipes/animal-type/animal-type.pipe';


import { DashboardComponent } from './component/dashboard/dashboard.component';
import { LoginComponent } from './component/login/login.component';
import { NavbarComponent } from './component/navbar/navbar.component';
import { AnimalsComponent } from './component/dashboard/animals/animals.component';
import { MessagesComponent } from './component/dashboard/messages/messages.component';
import { AddUserComponent } from './component/dashboard/add-user/add-user.component';
import { AddAnimalComponent } from './component/dashboard/animals/add-animal/add-animal.component';
import { AnimalTileComponent } from './component/dashboard/animals/animal-tile/animal-tile.component';
import { AnimalDetailsComponent } from './component/dashboard/animals/animal-details/animal-details.component';
import { HomeComponent } from './component/dashboard/home/home.component';
import { ShelterInfoManageComponent } from './component/dashboard/shelter-info-manage/shelter-info-manage.component';
import { VaccinationFormComponent } from './component/dashboard/animals/animal-details/vaccination-form/vaccination-form.component';
import { TreatmentFormComponent } from './component/dashboard/animals/animal-details/treatment-form/treatment-form.component';
import { AdoptionComponent } from './component/adoption/adoption.component';
import { AdoptionAnimalsComponent } from './component/adoption/adoption-animals/adoption-animals.component';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { AdoptionAnimalTileComponent } from './component/adoption/adoption-animal-tile/adoption-animal-tile.component';
import { AdoptionAnimalDetailsComponent } from './component/adoption/adoption-animal-details/adoption-animal-details.component';
import { ContactFormComponent } from './component/adoption/adoption-animal-details/contact-form/contact-form.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    NavbarComponent,
    AnimalsComponent,
    MessagesComponent,
    AddUserComponent,
    AddAnimalComponent,
    AnimalTileComponent,
    AnimalStatePipe,
    AnimalTypePipe,
    AnimalDetailsComponent,
    HomeComponent,
    ShelterInfoManageComponent,
    VaccinationFormComponent,
    TreatmentFormComponent,
    AdoptionComponent,
    AdoptionAnimalsComponent,
    NotFoundComponent,
    AdoptionAnimalTileComponent,
    AdoptionAnimalDetailsComponent,
    ContactFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    AuthService,
    AnimalService,
    PhotoService,
    ShelterService,
    NotificationService,
    MessageService,
    DocumentsService,
    AuthGuard,
    AdminGuard,
    AnimalDetailsGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
