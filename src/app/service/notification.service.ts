import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { apiUrl } from "./api-url";

@Injectable()
export class NotificationService {

  constructor(private http: Http) { }

  getMissingInfo(id: number) {
    return this.http.get(apiUrl + '/notifications/info/' + id).toPromise();
  }
  getNextVaccinations(id: number) {
    return this.http.get(apiUrl + '/notifications/vacc/' + id).toPromise();
  }
  getNextTreatments(id: number) {
    return this.http.get(apiUrl + '/notifications/treat/' + id).toPromise();
  }
}