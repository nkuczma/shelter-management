import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { User } from "app/model/user.model";
import { UserData } from "app/model/user-data.model";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Router } from '@angular/router';
import { ReplaySubject }    from 'rxjs/ReplaySubject';
import { apiUrl } from "./api-url";

@Injectable()
export class AuthService {

  constructor(private http: Http, private router: Router) { }
  
  isLogged: boolean;
  userData: UserData;

//   createUser(user: User): Promise<any> {
//     return this.http.post(apiUrl + '/users', user).toPromise();
//   }

  login(user: User): Promise<any> {
    return this.http.post(apiUrl + '/sessions/create', user).toPromise();
  }
  logout(){
    localStorage.removeItem('id_token');
    this.router.navigate(['/login']);
    this.isLogged = false;
    this.userData = null;
  }
  setUserData(user: UserData) {
      this.userData = user;
  }
  createUser(user): Promise<any> {
    return this.http.post(apiUrl + '/users', user).toPromise();
  }
  checkToken(token): Observable<boolean> | boolean {
    let obs;
    try {
    obs = this.http.post(apiUrl + '/users/check', token)
      .map( res => res.json() )
      .map(json => { 
        if(json.exists) console.log(json.role); this.setUserData({shelter_id: json.shelter_id, user_name: json.user_name, user_role: json.role}); this.isLogged = true; return json.exists; 
      }).catch((response) => { this.router.navigate(['/login']); this.isLogged = false; return response._body; });
    } catch (err) {
      obs = Observable.of(false);  
    }
    return obs.map(success => {
      if (!success)
        this.router.navigate(['/login']);
        this.isLogged = false;
        return success;
    });
  }
  getUserData(): UserData {
    return this.userData;
  }
  isAdmin(): boolean {
    if(this.userData.user_role==="admin") { return true; }
    else { return false; }
  }
}

