import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { Animal } from "app/model/animal.model";
import { apiUrl } from "./api-url";

@Injectable()
export class PhotoService {

  constructor(private http: Http) { }

  getAnimalPhoto(id: number) {
    return this.http.get(apiUrl + '/photos/' + id).toPromise();
  }

  postAnimalPhoto(formData, id) {
    let headers = new Headers();
    headers.append("Authorization", localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(apiUrl + '/photos/add/' + id, formData, options).toPromise();
  }
}