import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { apiUrl } from "./api-url";

@Injectable()
export class MessageService {

  constructor(private http: Http) { }

  postMessage(message) {
    return this.http.post(apiUrl + '/message/new', message).toPromise();
  }
  getUnansweredMessages(id) {
    let headers = new Headers();
    headers.append("Authorization", localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });
    return this.http.get(apiUrl + '/messages/unanswered/' + id, options).toPromise();
  }
  getAllMessages(id) {
    let headers = new Headers();
    headers.append("Authorization", localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });
    return this.http.get(apiUrl + '/messages/all/' + id, options).toPromise();
  }
  editMessage(message, id) {
    return this.http.post(apiUrl + '/message/edit/' + id, message).toPromise();
  }

  getUserMessages(data) {
    return this.http.post(apiUrl + '/messages/user', data).toPromise();
  }
}