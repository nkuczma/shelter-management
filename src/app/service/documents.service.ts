import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, ResponseContentType } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { apiUrl } from "./api-url";

@Injectable()
export class DocumentsService {

  constructor(private http: Http) { }

  getAnimalDocument(animal, shelter) {
    var data: any = {};
    data.token = localStorage.getItem('id_token');
    data.name_full = shelter.name_full;
    data.address = shelter.address;
    data.city = shelter.city;
    data.registry_number = animal.registry_number;
    data.tatoo_id = animal.tatoo_id;
    data.species = animal.species;
    data.microchip = animal.microchip;
    return this.http.post(apiUrl + '/docs/adoption', data, { responseType: ResponseContentType.Blob })
        .map((res) => {
            return new Blob([res.blob()], { type: 'application/vnd.openxmlformats-officedocument.documentml.document' })
        });
  }
}