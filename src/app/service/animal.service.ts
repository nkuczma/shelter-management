import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { Animal } from "app/model/animal.model";
import { apiUrl } from "./api-url";


@Injectable()
export class AnimalService {

  constructor(private http: Http) { }

  addAnimal(animal): Promise<any> {
    let data = animal;
    data.token = localStorage.getItem('id_token');
    return this.http.post(apiUrl + '/animals/add', data).toPromise();
  }

  getAnimalsByShelterId(id) : Promise<Array<Animal>>{
    return this.http.get(apiUrl + '/animals/' + id)
    .map((response) => { return response.json() })
    .toPromise();
  }

  getAnimalByAnimalId(id) : Promise<Animal>{
    return this.http.get(apiUrl + '/animal/' + id)
    .map((response) => { return response.json() })
    .toPromise();
  }

  editAnimal(animalData, animalId): Promise<any> {
    let data = animalData;
    data.token = localStorage.getItem('id_token');
    console.log(data);
    return this.http.post(apiUrl + '/animals/edit/' + animalId, data).toPromise();
  }

  addVaccination(vacc, id): Promise<any> {
    let data = vacc;
    data.token = localStorage.getItem('id_token');
    data.animal_id = id;
    return this.http.post(apiUrl + '/vaccination/add', data).toPromise();
  }

  editVaccination(vacc, id): Promise<any> {
    let data = vacc;
    data.token = localStorage.getItem('id_token');
    return this.http.post(apiUrl + '/vaccination/edit/' + id, data).toPromise();
  }

  getVaccinationByAnimalId(id) : Promise<any>{
    return this.http.get(apiUrl + '/vaccination/' + id)
    .map((response) => { return response.json() })
    .toPromise();
  }

  addTreatment(vacc, id): Promise<any> {
    let data = vacc;
    data.token = localStorage.getItem('id_token');
    data.animal_id = id;
    return this.http.post(apiUrl + '/treatment/add', data).toPromise();
  }
  
  editTreatment(treat, id): Promise<any> {
    let data = treat;
    data.token = localStorage.getItem('id_token');
    return this.http.post(apiUrl + '/treatment/edit/' + id, data).toPromise();
  }

  getTreatmentByAnimalId(id) : Promise<any>{
    return this.http.get(apiUrl + '/treatment/' + id)
      .map((response) => { return response.json() }).toPromise();
  }

  getAnimalsBySpeciesProvince(province, species) {
    return this.http.get(apiUrl + '/animals/adoption/' + province + "/" + species)
      .map((response) => { return response.json() }).toPromise();
  }

  getAnimalAdoption(id) {
    return this.http.get(apiUrl + '/adoption/animal/' + id)
      .map((response) => { return response.json() }).toPromise();
  }
}