import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from "@angular/http";
import "rxjs/Rx";
import { Observable } from "rxjs/Rx";
import { Shelter } from "app/model/shelter.model";
import { apiUrl } from "./api-url";

@Injectable()
export class ShelterService {

  constructor(private http: Http) { }

  getShelter(id: number) {
    return this.http.get(apiUrl + '/shelter/' + id).toPromise();
  }

  editShelter(shelter: Shelter, id: number) {
    console.log(id);
    let headers = new Headers();
    headers.append("Authorization", localStorage.getItem('id_token'));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(apiUrl + '/shelter/edit/' + id, shelter, options).toPromise();
  }

  getProvinceList() {
    return this.http.get(apiUrl + '/provinces').toPromise();
  }
}