import { Routes } from '@angular/router';
import { DashboardComponent } from "./component/dashboard/dashboard.component";
import { LoginComponent } from './component/login/login.component';
import { AnimalsComponent } from './component/dashboard/animals/animals.component';
import { MessagesComponent } from './component/dashboard/messages/messages.component';
import { AddUserComponent } from './component/dashboard/add-user/add-user.component';
import { AddAnimalComponent } from './component/dashboard/animals/add-animal/add-animal.component';
import { AnimalDetailsComponent } from './component/dashboard/animals/animal-details/animal-details.component';
import { HomeComponent } from './component/dashboard/home/home.component';
import { ShelterInfoManageComponent } from './component/dashboard/shelter-info-manage/shelter-info-manage.component';
import { AdoptionComponent } from './component/adoption/adoption.component';
import { AdoptionAnimalsComponent } from './component/adoption/adoption-animals/adoption-animals.component';
import { NotFoundComponent } from './component/not-found/not-found.component';
import { AdoptionAnimalDetailsComponent } from './component/adoption/adoption-animal-details/adoption-animal-details.component';

import { AuthGuard } from './guard/auth.guard';
import { AdminGuard } from './guard/admin.guard';
import { AnimalDetailsGuard } from './guard/animal-details.guard';

export const routes: Routes = [
    { path: '', redirectTo: 'adoption', pathMatch: 'full' },
    { 
        path: 'manage',
        component: DashboardComponent,
        canActivate: [AuthGuard],
        children: [
            { path: 'home', component: HomeComponent },
            { path: 'animals', component: AnimalsComponent },
            { path: 'animals/add', component: AddAnimalComponent },
            { path: 'animals/:shelterId/:animalId', component: AnimalDetailsComponent, canActivate: [AnimalDetailsGuard] },
            { path: 'messages', component: MessagesComponent },
            { path: 'shelter-info', component: ShelterInfoManageComponent },
            { path: 'user-add', component: AddUserComponent, canActivate: [AdminGuard] }
          ]
    },
    { path: 'login', component: LoginComponent },
    { path: 'adoption', 
    component: AdoptionComponent,
    children: [
        { path: '', component: AdoptionAnimalsComponent },
        { path: 'animal/:animalId', component: AdoptionAnimalDetailsComponent },
    ]
    },
    { path: '**', component: NotFoundComponent }
];