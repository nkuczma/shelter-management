import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from "../service/auth.service";

@Injectable ()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private loginService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.loginService.isLogged) { return true; }
    else if(!localStorage.getItem('id_token')) { 
      this.router.navigate(['/login']); 
      return false; 
    }
    else { 
      let token = { token: localStorage.getItem('id_token')};
      return this.loginService.checkToken(token); 
    }
  }
}