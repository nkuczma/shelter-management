import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from "../service/auth.service";

@Injectable ()
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private loginService: AuthService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.loginService.getUserData().user_role==="admin") {
        return true;
    }
    else {
        this.router.navigate(['/manage/home']); 
        return false;
    }
  }
}