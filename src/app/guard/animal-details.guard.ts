import { Injectable } from '@angular/core';
import { Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { AuthService } from "../service/auth.service";

@Injectable ()
export class AnimalDetailsGuard implements CanActivate {

  constructor(private router: Router, private authService: AuthService) { 
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.authService.getUserData().shelter_id===route.params.shelterId) {
        return true;
    }
    else {
      this.router.navigate(['/manage/home']);
      return false;
    }
  }
}