import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'animalState'
})
export class AnimalStatePipe implements PipeTransform {

  transform(value: string): string {
    if(value==="for_adopt") return "Do adopcji";
    else if(value==="adopted") return "Zaadoptowany";
    else if(value==="dead") return "Zmarły";
    else return "Nieznany";
  }

}
