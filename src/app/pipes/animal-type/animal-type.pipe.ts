import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'animalType'
})
export class AnimalTypePipe implements PipeTransform {

  transform(value: string): string {
    if(value==="dog") return "Pies";
    else if(value==="cat") return "Kot";
    else return "Inny";
  }

}
