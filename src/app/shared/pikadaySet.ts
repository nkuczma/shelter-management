export const i18n = 
{
    previousMonth : 'Poprzedni Miesiąc',
    nextMonth     : 'Następny Miesiąc',
    months        : ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
    weekdays      : ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
    weekdaysShort : ['Nd','Pn','Wt','Śr','Cz','Pt','Sb']
}