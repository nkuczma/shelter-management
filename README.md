# ShelterManagement

The purpose of this application was to allow managing work at an animal shelter. There are two parts of app - first for workers, and second for people looking for an animal to adoption.

## Used technologies

Application was made in Angular 4, using RxJS.

## For workers

In the first part, for the shelter’s workers, users can add and edit informations about animals, their’s diseases etc. There are 3 subpages for every shelter worker - MainPage - where user can view reminders for vaccinations and treatments, Animals - where are profiles of every animal in shelter and Messages - where user can view messages directed to shelter. Two other views available only for admin of the shelter - AddNewUser - for adding new shelter worker, and ShelterInfo where admin can change shelter's phone, adress, etc.

![](https://lh3.googleusercontent.com/znnmrIEvImEgPhDXxAP5oIKSlI-wK9gCsXH1KVb-oC-qXOIORmBT87CaM_4xlSPjLotCGRi3zGy5uX_87ynO=w1366-h603)

![](https://lh3.googleusercontent.com/sRdnI6ZsMDHSaEbmv_0O2dt_VGKzwQU_Zan6-e9KiQmVu00XJ5k3PXrrMg2JfOydpxnKPs3T69uruvv0n-X3=w1366-h603)

## For adoption

Second part contains service available for everyone, where users can view animals available for adoption from their region. Profiles of animals are taken from every shelter taking part in the system, so shelter workers can keep animals register and don't have to do any extra work to show animals than can be adopted. People interested in adoption can send a message, which will be displayed in Messages subpage of this specific shelter.

![](https://lh5.googleusercontent.com/tWTfzAcu2wYykVn4E3wctVUet-ySUOVGJFyID9a13WUufWNYsyloUtrM-zR7R7eBq3ktVQDM2pHyWfbg1ogp=w1366-h603)

## Launching

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 